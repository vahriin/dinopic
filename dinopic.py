import sys
from PIL import Image, ImageDraw

with Image.open(sys.argv[1]) as i_dino0, Image.open(sys.argv[2]) as i_dino1:
    width, height = i_dino0.size # the sizes of pictures are identical
    dino0, dino1 = i_dino0.load(), i_dino1.load()
    for i in range(width):
        for j in range(height):
            dino0[i, j] = (tuple(map(lambda x, y: abs(x - y), dino0[i,j], dino1[i,j])))
    i_dino0.save('dinoresult.jpg', 'JPEG')
